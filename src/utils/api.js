import axios from "axios";

class API {
  axiosInstance = null;

  constructor() {
    /* 
      🚨1 point EXTRA CREDIT 🚨 👉🏿 get the baseURL from the environment
      https://create-react-app.dev/docs/adding-custom-environment-variables/
    */
    const axiosInstance = axios.create({
      baseURL: "https://kwitter-api.herokuapp.com/",
      timeout: 30000,
      headers: { Authorization: `Bearer ${getToken()}` },
    });

    // Add a request interceptor to attach a
    axiosInstance.interceptors.request.use(
      (config) => ({
        ...config,
        headers: {
          ...config.headers,
          Authorization: `Bearer ${getToken()}`,
        },
      }),
      (error) => Promise.reject(error)
    );

    // Add a response interceptor
    axiosInstance.interceptors.response.use(
      ({ data }) => data,
      (error) => Promise.reject(error)
    );

    this.axiosInstance = axiosInstance;
  }

  async login({ username, password }) {
    try {
      const result = await this.axiosInstance.post("/auth/login", {
        username,
        password,
      });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async logout() {
    try {
      await this.axiosInstance.get("/auth/logout");
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async loginGoogle(googleData) {
    try {
      const result = await this.axiosInstance.get("/auth/google/login")
      console.log("Google Result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async createUser({ username, password, displayName }) {
    try {
      const result = await this.axiosInstance.post("/users", {
        username,
        password,
        displayName
      });
      console.log('Create User Result', result)
      return result
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async updateUser({ username }) {
    try {
      const result = await this.axiosInstance.patch(`/users/${username}`);
      return result
      // console.log("Result", result)
    } catch (err) {
      helpMeInstructor(err);
      throw err
    }
  }

  async deleteUser({ username }) {
    try {
      const result = await this.axiosInstance.delete(`/users/${username}`);
      return result;
      console.log(result)
    } catch (err) {
      helpMeInstructor(err);
      throw err
    }
  }

  async postMessage(text) {
    try {
      const result = await this.axiosInstance.post("/messages",
        text
      );
      console.log('Post Message Result', result)
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async deleteMessage(id) {
    try {
      const result = await this.axiosInstance.delete(`/messages/${id}`);
      console.log("Chaching, message #" + id + " deleted!")
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async getProfileMessages(username) {
    try {
      const result = await this.axiosInstance.get(`/messages?limit=10&username=${username}`)
      console.log("Profile Messages Result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async getMessageFeed() {
    try {
      const result = await this.axiosInstance.get("/messages?limit=10")
      console.log("Message Feed Result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async getUser(username) {
    try {
      const result = await this.axiosInstance.get(`/users/${username}`)
      console.log("Get User Result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async putUserPicture(username, data) {
    try {
      const result = await this.axiosInstance.put(`/users/${username}/picture`, 
      data)
      console.log("Get User Result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async getMessage(id) {
    try {
      const result = await this.axiosInstance.get(`/messages/${id}`)
      console.log("Get Message Result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async likeMessage(id) {
    try {
      const result = await this.axiosInstance.post("/likes", { "messageId": id })
      console.log("likes api result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }

  async dislikeMessage(id) {
    try {
      const result = await this.axiosInstance.delete(`/likes/${id}`)
      console.log("api dislikeId", id)
      console.log("dislikes api result", result)
      return result
    } catch (err) {
      helpMeInstructor(err)
      throw err
    }
  }
}


// WARNING.. do not touch below this line if you want to have a good day =]

function helpMeInstructor(err) {
  console.info(
    `
    Did you hit CORRECT the endpoint?
    Did you send the CORRECT data?
    Did you make the CORRECT kind of request [GET/POST/PATCH/DELETE]?
    Check the Kwitter docs 👉🏿 https://kwitter-api.herokuapp.com/docs/#/
    Check the Axios docs 👉🏿 https://github.com/axios/axios
    TODO: troll students
  `,
    err
  );
}

function getToken() {
  try {
    const storedState = JSON.parse(localStorage.getItem("persist:root"));
    return JSON.parse(storedState.auth).isAuthenticated;
  } catch {
    return "";
  }
}

export default new API();









  