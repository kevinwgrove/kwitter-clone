import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/users";
import { Loader } from "../loader";
import './RegistrationForm.css';
import { Link } from 'react-router-dom';
import { MenuContainer } from '../menu';
import { Button,
  TextField,
   } from '@material-ui/core'



export const RegisterForm = () => {
  

  const dispatch = useDispatch();

  const [state, setState] = useState({
    username: "",
    password: "",
    displayName: ""
  });

  const handleUser = (event) => {
    event.preventDefault();
    dispatch(actions.createUser(state));
    setState({
      username: "",
      password: "",
      displayName: ""
    })
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <React.Fragment>
      <MenuContainer/>
      
        <Link to='/' > Home </Link>
        
      <form id="registration-form" onSubmit={handleUser}>
      <label htmlFor="username"></label>
        <TextField
          type="text"
          name="username"
          value={state.username}
          autoFocus
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="Username"
        />

        <label htmlFor="password"></label>
        <TextField
          type="password"
          name="password"
          value={state.password}
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="Password"
          style={{marginTop: 15+'px'}}
        />
        <label htmlFor="displayName"></label>
        <TextField
          type="text"
          name="displayName"
          value={state.displayName}
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="DisplayName"
          style={{marginTop: 15+'px'}}
        />
        <Button 
        type="submit"
        variant="contained"
        style={{marginTop: 15+'px'}}
         >
          Create User
        </Button>
      </form>
      {/* {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>} */}
    </React.Fragment>
  );
};

