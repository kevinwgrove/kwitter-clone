import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { HomeScreen, 
  ProfileScreen, 
  NotFoundScreen, 
  RegistrationScreen, 
  MessageFeedScreen, 
  UpdateUserScreen } from "../../screens";
import { ConnectedRoute } from "../connected-route/ConnectedRoute";


export const Navigation = (state) => {
  return (
  <BrowserRouter>
    <Switch>
      <ConnectedRoute
        exact
        path="/"
        redirectIfAuthenticated
        component={HomeScreen}
      />
      
      <ConnectedRoute
        exact
        isProtected
        path="/profiles/:username"
        component={ProfileScreen}
      />

      <ConnectedRoute
        exact
        path="/registration"
        component={ RegistrationScreen } 
      />
    
      <ConnectedRoute 
        exact
        path="/messagefeed"
        component={ MessageFeedScreen }
      />

      <ConnectedRoute 
        exact 
        path="/updateuser"
        component={ UpdateUserScreen }
      />
      <ConnectedRoute path="*" component={NotFoundScreen} />
    </Switch>
  </BrowserRouter>
)};
