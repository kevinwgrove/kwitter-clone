import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/auth";
import { Loader } from "../loader";
import "./LoginForm.css";
import { Button } from '@material-ui/core';
import { TextField } from '@material-ui/core';



export const LoginForm = ({ login }) => {
  const { loading, error, loginGoogle } = useSelector((state) => ({
    loading: state.auth.loading,
    error: state.auth.error,
  }));

  const dispatch = useDispatch();

  const [state, setState] = useState({
    username: "",
    password: "",
  });

  const handleLogin = (event) => {
    event.preventDefault();
    dispatch(actions.login(state));
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  const startAuthentication = () => {

    const authWindow = window.open(
      "https://kwitter-api.herokuapp.com/auth/google/login",
      "_blank",
      "width=500,height=500"
    );
    authWindow.window.opener.onmessage = (event) => {
      if(!event || !event.data || !event.data.token) {
        return
      }
      authWindow.close()
      console.log("EVENT_DATA", event.data)
      dispatch(actions.googleLogin(event.data));
    };
  };

  return (
    <React.Fragment>
      <form 
      id="login-form" 
      onSubmit={handleLogin}
      >
        <label htmlFor="username"></label>
        <TextField
          type="text"
          name="username"
          value={state.username}
          autoFocus
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="Username"
        />

        <label htmlFor="password"></label>
        <TextField
          type="password"
          name="password"
          value={state.password}
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="Password"
          style={{marginTop: 15+'px'}}
        />
        
        <Button 
        type="submit" 
        disabled={loading}
        onSubmit={handleLogin}
        variant="contained"
        style={{marginTop: 15+'px'}}
        >
          Login
        </Button>
        <Button 
        variant="contained"
        style={{marginTop: 15+'px', marginBottom: 5+'px'}}
        onClick={startAuthentication}>Google Login</Button>
      </form>
      {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>}

    </React.Fragment>
  );
};
