import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/users";
import { Loader } from "../loader";
import './UpdateUser.css';
import { MenuContainer } from '../menu';
import { Button } from '@material-ui/core'



export const UpdateUserForm = () => {
  

  const dispatch = useDispatch();

  const [state, setState] = useState({
    password: "",
    displayName: ""
  });

  const handleUser = (event) => {
    event.preventDefault();
    dispatch(actions.updateUser(state));
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <React.Fragment>
      <MenuContainer/>
        
      <form id="update-user-form" onSubmit={handleUser}>
        {/* <label htmlFor="username">Username</label>
        <input
          type="text"
          name="username"
          value={state.username}
          autoFocus
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="Username"
        /> */}
        <label htmlFor="password">Password</label>
        <input
          type="password"
          name="password"
          value={state.password}
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="Password"
          
        />
        <label htmlFor="displayName">Display Name</label>
        <input
          type="text"
          name="displayName"
          value={state.displayName}
          required
          onChange={handleChange}
          id="outlined-basic"
          variant="outlined"
          label="DisplayName"
        />
        <Button 
        type="submit"
        variant="contained"
        style={{marginTop: 15+'px'}}
         >
          Update User
        </Button>
      </form>
      {/* {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>} */}
    </React.Fragment>
  );
};
