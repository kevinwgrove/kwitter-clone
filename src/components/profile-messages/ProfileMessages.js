import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/messages";
import { IconButton, 
    List, 
    ListItem, 
    Divider, 
    ListItemText, 
    ListItemAvatar, 
    Avatar, 
    Tooltip } from '@material-ui/core';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { v4 as uuidv4 } from 'uuid'

export const ProfileMessages = () => {
    const profileMessages = useSelector((state) => state.getProfileMessages.profileMessages)
    const username = useSelector((state) => state.auth.username)
    const dispatch = useDispatch();

    useEffect(() =>  {
        dispatch(actions.getProfileMessages(username))
        console.log(profileMessages)        
    },[])

    const handleDeleteMessage = (id) => {
        dispatch(actions.deleteMessage(id));
        console.log('messageID', id)
        setTimeout(() => dispatch(actions.getProfileMessages(username)), 200)
      };

      return (
        <>
            <List>
                {profileMessages.map(message => {
                return (
                <>      
                        <ListItem 
                            alignItems="flex-start" 
                            key={uuidv4()} 
                            style={{margin: 10+'px ' + 0}}
                        >
                            <ListItemAvatar 
                                key={uuidv4()} 
                            >
                                <Avatar 
                                    key={uuidv4()}
                                    alt={message.username} 
                                    src={"https://kwitter-api.herokuapp.com/users/" + message.username + "/picture"} 
                                /> 
                            </ListItemAvatar>
                            <ListItemText 
                                primary={message.username}
                                secondary={message.text}
                                key={uuidv4()}
                            />
                                <IconButton 
                                    size="small" 
                                    key={uuidv4()}
                                    style={{marginRight: 15+'px', marginTop: 12+'px'}}
                                    onClick={() => handleDeleteMessage(message.id)} 
                                >
                                    <Tooltip 
                                        title="Delete"
                                        placement="top"
                                        key={uuidv4()} 
                                    >
                                        <DeleteForeverIcon
                                            color="error"
                                            key={uuidv4()}
                                            className="delete-icon"
                                        />
                                </Tooltip>  
                            </IconButton>         
                        </ListItem>
                    <Divider/>
                </>
                )}
                )}
            </List>
        </>
    )
}