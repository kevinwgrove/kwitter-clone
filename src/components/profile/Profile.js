import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch }   from'react-redux';
import { Backdrop, 
    Modal, 
    TextField, 
    Button, 
    Card, 
    CardActions, 
    CardContent, 
    CardMedia, 
    Typography } from '@material-ui/core';
import {actions} from '../../redux/actions/users';
import { useSpring, animated } from 'react-spring/web.cjs';
import {actions as authActions} from '../../redux/actions/auth';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

//https://codesandbox.io/s/y42h3?file=/demo.js:724-1227
const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      display: 'flex',
      flexDirection: 'column'
    },
}));

//https://codesandbox.io/s/y42h3?file=/demo.js:724-1227
const Fade = React.forwardRef(function Fade(props, ref) {
const { in: open, children, onEnter, onExited, ...other } = props;
const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
    if (open && onEnter) {
        onEnter();
    }
    },
    onRest: () => {
    if (!open && onExited) {
        onExited();
    }
    },
});

return (
    <animated.div ref={ref} style={style} {...other}>
    {children}
    </animated.div>
);
});

//https://codesandbox.io/s/y42h3?file=/demo.js:724-1227
Fade.propTypes = {
children: PropTypes.element,
in: PropTypes.bool.isRequired,
onEnter: PropTypes.func,
onExited: PropTypes.func,
};




export const Profile = () => {
    const myUsername = useSelector((state) => state.auth.username);
    // const password = useSelector((state) => state.auth.password);
    // const myDisplayName = useSelector((state) => state.users.displyName)
    const dispatch = useDispatch()
    const classes = useStyles();
    const form = useRef(null)
    const [open, setOpen] = useState(false);

    
    const handleDeleteUser = (event) => {
        event.preventDefault();
        dispatch(actions.deleteUser(myUsername));
        dispatch(authActions.logout())
      };

    const handlePutPicture = (event) => {
        event.preventDefault()
        let data = new FormData(form.current)
        console.log("handlePutPicture data", data)
        dispatch(actions.putUserPicture(myUsername, data))
        handleClose()
        setTimeout(() => window.location.reload(), 500)
    }

    const handleOpen = () => {
        setOpen(true);
        console.log("handleOpen state", open)
      };

    const handleClose = () => {
        setOpen(false);
    };

    // const getUser = (username) => {
    //     dispatch(actions.getUser(username))
    // }
    

return (
    <Card>
        <CardMedia 
        style={{
            height: 250+'px',
            width: 250+'px'
        }}
        image={"https://kwitter-api.herokuapp.com/users/" + myUsername + "/picture"}
        />

        <CardContent>

            <Typography 
                gutterBottom variant='h5' 
                component='h2'
            >
                {myUsername}
            </Typography>

            <Typography 
                variant="body2" 
                color="textSecondary" 
                component="p"
            >
            </Typography>
        
        </CardContent>

        <CardActions>
            <Button 
                variant='contained' 
                color='default'
                onClick={handleOpen}
            >
                {/* <Link to="/updateuser">Update Profile</Link> */}
                Upload Photo
            </Button>

            <Modal
                aria-labelledby="spring-modal-title"
                aria-describedby="spring-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                timeout: 500,
                }}
            >
                <Fade in={open}>
                
                        <div className={classes.paper}>
                            <form 
                                ref={form} 
                                onSubmit={(event) => handlePutPicture(event)}
                            >
                                <input 
                                    type="file"
                                    name="picture"
                                />

                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="default"
                                    className={classes.button}
                                    startIcon={<CloudUploadIcon />}
                                >
                                    Upload
                                </Button>
                            </form>
                        </div>
                    
                </Fade>
            </Modal>

            <Button 
                variant='contained' 
                color='secondary' 
                onClick={ handleDeleteUser }
            >
                Delete Profile
            </Button>
        </CardActions>

    </Card>
)
}
