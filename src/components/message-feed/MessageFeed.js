import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/messages";
import {
  IconButton,
  List,
  ListItem,
  Divider,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Tooltip,
} from "@material-ui/core";
import { actions as likeActions } from "../../redux/actions/likes";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { v4 as uuidv4 } from "uuid";
import "./MessageFeed.css";

export const MessageFeed = () => {
  const messageFeed = useSelector((state) => state.getMessageFeed.messageFeed);
  const myUsername = useSelector((state) => state.auth.username);
  const dispatch = useDispatch();
  // const likeId = useSelector((state) => state.MessageFeed.message)
  // const message = useSelector((state) => state.likeMessage.messageId)

  useEffect(() => {
    dispatch(actions.getMessageFeed());
  }, []);

  const handleDeleteMessage = (id) => {
    dispatch(actions.deleteMessage(id));
    console.log("messageID", id);
    setTimeout(() => dispatch(actions.getMessageFeed()), 200);
  };

  const handleLikeMessage = (id) => {
    console.log("handleLikeMessage id", id);

    dispatch(likeActions.likeMessage(id));
    // setTimeout(() => dispatch(actions.getMessageFeed()), 200)
  };
  
  const handleLike = (message) => {
     (likeBool(message)) ? handleDislikeMessage(message.id, message) : handleLikeMessage(message.id)
  }

  const likeBool = (message) => {
    for (let like in message.likes) {
      return myUsername === message.likes[like].username;
    }
  };

  const handleDislikeMessage = (id, message) => {
    console.log("HERE", id, message.likes);
    let likeId = 0;
    message.likes.forEach((like) => {
      if (like.username === myUsername) {
          likeId = like.id;
      }
  });
    dispatch(likeActions.dislikeMessage(likeId));
    console.log("dislikeMessage Id", message);
    // setTimeout(() => dispatch(actions.getMessageFeed()), 200)
  };

  return (
    <>
      <List>
        {messageFeed.map((message) => {
          return (
            <div key={uuidv4()}>
              <ListItem
                alignItems="flex-start"
                key={uuidv4()}
                style={{ margin: 10 + "px " + 0 }}
              >
                <ListItemAvatar key={uuidv4()}>
                  <Avatar
                    key={uuidv4()}
                    alt={message.username}
                    src={
                      "https://kwitter-api.herokuapp.com/users/" +
                      message.username +
                      "/picture"
                    }
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={message.username}
                  secondary={message.text}
                  key={uuidv4()}
                />
                {myUsername === message.username ? (
                  // <IconButton
                  //     key={uuidv4()}
                  //     onClick={() => handleDeleteMessage(message.id)}
                  // >
                  <IconButton
                    size="small"
                    key={uuidv4()}
                    style={{ marginRight: 15 + "px", marginTop: 12 + "px" }}
                    onClick={() => handleDeleteMessage(message.id)}
                  >
                    <Tooltip title="Delete" placement="top">
                      <DeleteForeverIcon
                        color="error"
                        key={uuidv4()}
                        className="delete-icon"
                        // style={{marginTop: 15+'px', marginRight: 15+'px'}}
                      />
                    </Tooltip>
                  </IconButton>
                ) : // </IconButton>
                null}
                
                <IconButton
                  size="small"
                  key={uuidv4()}
                  style={{ marginTop: 12 + "px" }}
                  onClick={() => handleLike(message)}
                >
                  {!likeBool(message) && (
                    <ThumbUpIcon
                      key={uuidv4()}
                      color="disabled"
                      className="thumb-up-icon"
                    />
                  )}
                  {likeBool(message) && (
                    <ThumbUpIcon
                      key={uuidv4()}
                      color="primary"
                      className="thumb-down-icon"
                    />
                  )}
                </IconButton>
              </ListItem>
              <Divider />
            </div>
          );
        })}
      </List>
    </>
  );
};



