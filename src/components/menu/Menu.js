import React from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/index";
import "./Menu.css";


export const Menu = () => {
  const isAuthenticated = useSelector((state) => !!state.auth.isAuthenticated);
  // const username = useSelector((state) => state.users.username)
  const dispatch = useDispatch();
  const logout = () => dispatch(actions.logout());
  return (
    <div 
    id="menu"  
    style={{marginBottom: 20+'px'}}>
      <h1>Kwitter</h1>
      <p>#BlackLivesMatter</p>
      <div id="menu-links">
        {isAuthenticated ? (
          <>
            
              
                <Link 
                to="/"
                style={{ textDecoration: 'none', color: "black" }}
                >
                  Profile
                </Link>
              

              
                <Link 
                to="/messagefeed"
                style={{ textDecoration: 'none', color: "black" }}
                >
                  Message Feed
                </Link>
              

              
                <Link 
                to="/" 
                style={{ textDecoration: 'none', color: "black" }}
                onClick={logout}
                >
                  Logout
                </Link>
              

            
          </>
        ) : null}
      </div>
    </div>
  );
};
