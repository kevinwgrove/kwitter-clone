import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { actions } from "../../redux/actions/messages";
import "./Message.css";
import { Button } from '@material-ui/core';
import { TextField } from '@material-ui/core'
import { MenuContainer } from "../menu";
import { MessageFeed } from "../message-feed/MessageFeed"


export const Message = () => {
  const { loading, error } = useSelector((state) => ({
    loading: state.auth.loading,
    error: state.auth.error,
  }));

  const dispatch = useDispatch();

  const [state, setState] = useState({
    text: ""
  });

  const handlePostMessage = (event) => {
    console.log('State', state)
    event.preventDefault();
    dispatch(actions.postMessage(state));
    setState({
      text: ''
    })
    setTimeout(() => dispatch(actions.getMessageFeed()), 500)
  };


  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  return (
    <React.Fragment>
      <form 
      id="message" 
      onSubmit={handlePostMessage} 
      style={{
        maxWidth: 100+'em', 
        marginBottom: 20+'px'}}
      >
       
        <TextField
          type="text"
          name="text"
          value={state.text}
          autoFocus
          onChange={handleChange}
          label="Type message here..."
          variant="outlined"
          style={{ 
            marginBottom: 10+'px'}}
        />
        <Button
        variant="contained"
        type="submit"
        id="message-button"
        >
          Post
        </Button>
      </form>

      {/* {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>} */}
    </React.Fragment>
  );
};
