import api from "../../utils/api";
import {actions as getMessageFeed} from "./messages"

export const LIKE_MESSAGE = "LIKE/LIKE_MESSAGE";
export const LIKE_MESSAGE_SUCCESS = "LIKE/LIKE_MESSAGE_SUCCESS";
export const LIKE_MESSAGE_FAILURE = "LIKE/LIKE_MESSAGE_FAILURE";
export const DISLIKE_MESSAGE = "LIKE/DISLIKE_MESSAGE";
export const DISLIKE_MESSAGE_SUCCESS = "LIKE/DISLIKE_MESSAGE_SUCCESS";
export const DISLIKE_MESSAGE_FAILURE = "LIKE/DISLIKE_MESSAGE_FAILURE";

const _likeMessage = (id) => async (dispatch, getState, message) => {
    try {
        dispatch({ type: LIKE_MESSAGE });
        const payload = await api.likeMessage(id);
        console.log('like message', payload);
        dispatch({ type: LIKE_MESSAGE_SUCCESS, payload})
        console.log(message.likes.length)
    } catch (err) {
        dispatch({
            type: LIKE_MESSAGE_FAILURE,
            payload: err.message,
        });
    }
}

const likeMessage = (id) => (dispatch, getState, message) => {
    return dispatch(_likeMessage(id))
        .then(() => {return dispatch(getMessageFeed.getMessageFeed())})
}

const _dislikeMessage = (id) => async (dispatch, getState) => {
    try {
        dispatch({ type: DISLIKE_MESSAGE})
        console.log("actions dislike id", id)
        const payload = await api.dislikeMessage(id);
        console.log("dislike message", payload);
        dispatch({ type: DISLIKE_MESSAGE_SUCCESS, payload})
        
    } catch (err) {
        dispatch({
            type: DISLIKE_MESSAGE_FAILURE,
            payload: err.message,
        });
    }
}

const dislikeMessage = (id) => (dispatch, getState, message) => {
    return dispatch(_dislikeMessage(id))
        .then(() => {return dispatch(getMessageFeed.getMessageFeed())})
}

export const actions = {likeMessage, dislikeMessage}