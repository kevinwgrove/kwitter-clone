import api from "../../utils/api";

// AUTH CONSTANTS
export const POST_MESSAGE = "MESSAGE/POST_MESSAGE";
export const POST_MESSAGE_SUCCESS = "MESSAGE/POST_MESSAGE_SUCCESS";
export const POST_MESSAGE_FAILURE = "MESSAGE/POST_MESSAGE_FAILURE";
export const DELETE_MESSAGE = "MESSAGE/DELETE_MESSAGE";
export const DELETE_MESSAGE_SUCCESS = "MESSAGE/DELETE_MESSAGE_SUCCESS";
export const DELETE_MESSAGE_FAILURE = "MESSAGE/DELETE_MESSAGE_FAILURE";
export const GET_MESSAGE_FEED = "MESSAGE/GET_MESSAGE_FEED"
export const GET_MESSAGE_FEED_SUCCESS = "MESSAGE/GET_MESSAGE_FEED_SUCCESS"
export const GET_MESSAGE_FEED_FAILURE = "MESSAGE/GET_MESSAGE_FEED_FAILURE"
export const GET_PROFILE_MESSAGE_FEED = "MESSAGE/GET_PROFILE_MESSAGE_FEED"
export const GET_PROFILE_MESSAGE_FEED_SUCCESS = "MESSAGE/GET_PROFILE_MESSAGE_FEED_SUCCESS"
export const GET_PROFILE_MESSAGE_FEED_FAILURE = "MESSAGE/GET_PROFILE_MESSAGE_FEED_FAILURE"
/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/
// const getMessageFeed = 

const postMessage = (text) => async (dispatch, getState) => {
  try {
    dispatch({ type: POST_MESSAGE });
    const payload = await api.postMessage(text);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: POST_MESSAGE_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: POST_MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};

const deleteMessage = (id) => async (dispatch, getState) => {
  try {
    dispatch({ type: DELETE_MESSAGE });
    const payload = await api.deleteMessage(id);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: DELETE_MESSAGE_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: DELETE_MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};

const getMessageFeed = () => async (dispatch, getState) => {
  try {
    dispatch({ type: GET_MESSAGE_FEED });
    const payload = await api.getMessageFeed();
    console.log('Get message feed Payload', payload)
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    dispatch({ type: GET_MESSAGE_FEED_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: GET_MESSAGE_FEED_FAILURE,
      payload: err.message,
    });
  }
};

const getProfileMessages = (username) => async (dispatch, getState) => {
  try {
    console.log("Get Profile Messages",username)
    dispatch({ type: GET_PROFILE_MESSAGE_FEED });
    const payload = await api.getProfileMessages(username);
    console.log('Get message feed Payload', payload)
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    dispatch({ type: GET_PROFILE_MESSAGE_FEED_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: GET_PROFILE_MESSAGE_FEED_FAILURE,
      payload: err.message,
    });
  }
};

export const actions = {
  postMessage,
  deleteMessage,
  getMessageFeed,
  getProfileMessages
};
