import api from '../../utils/api'
export const CREATE_USER = 'USERS/CREATE_USER'
export const CREATE_USER_SUCCESS = "USERS/CREATE_USER_SUCCESS";
export const CREATE_USER_FAILURE = "USERS/CREATE_USER_FAILURE";
export const UPDATE_USER = 'USERS/UPDATE_USER'
export const UPDATE_USER_SUCCESS = "USERS/UPDATE_USER_SUCCESS";
export const UPDATE_USER_FAILURE = "USERS/UPDATE_USER_FAILURE";
export const DELETE_USER = "USERS/DELETE_USER";
export const DELETE_USER_SUCCESS = "USERS/DELETE_USER_SUCCESS";
export const DELETE_USER_FAILURE = "USERS/DELETE_USER_FAILURE";
export const PUT_USER_PICTURE = "USERS/PUT_USER_PICTURE";
export const PUT_USER_PICTURE_SUCCESS = "USERS/PUT_USER_PICTURE_SUCCESS";
export const PUT_USER_PICTURE_FAILURE = "USERS/PUT_USER_PICTURE_FAILURE";


const createUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: CREATE_USER });
    const payload = await api.createUser(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    dispatch({ type: CREATE_USER_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: CREATE_USER_FAILURE,
      payload: err.message,
    });
  }
};


const deleteUser = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: DELETE_USER });
    // console.log('DeleteUserCredentials', credentials)
    const payload = await api.deleteUser(credentials);
    // console.log('DeleteUserPayload', payload)
    dispatch({ type: DELETE_USER_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: DELETE_USER_FAILURE,
      payload: err.message,
    });
  }
};

const updateUser = (displayName, password) => async (dispatch, getState) => {
  try {
    dispatch({ type: UPDATE_USER });
    const payload = await api.updateUser(displayName, password);
    dispatch({ type: UPDATE_USER_SUCCESS, payload});
  } catch (err) {
    dispatch({
      type: UPDATE_USER_FAILURE,
      payload: err.message,
    });
  }
};

const putUserPicture = (username, data) => async (dispatch, getState) => {
  try {
    dispatch({ type: PUT_USER_PICTURE });
    const payload = await api.putUserPicture(username, data);
    console.log("Get User Payload", payload)
    dispatch({ type: PUT_USER_PICTURE_SUCCESS, payload });
  } catch (err) {
    console.log("Get User Error Status Code", err.statusCode)
    dispatch({
      type: PUT_USER_PICTURE_FAILURE,
      payload: err.message,
    });
  }
}

export const actions = {
  createUser,
  deleteUser,
  putUserPicture,
  updateUser
};