import { combineReducers } from "redux";
import { authReducer } from "./auth";
import { usersReducer } from "./users";
import { postMessageReducer } from "./postMessage";
import { deleteMessageReducer } from "./deleteMessage"
import { getMessageFeedReducer } from "./getMessageFeed"
import { getProfileMessagesReducer } from "./getProfileMessageFeed"
import { likeMessageReducer, dislikeMessageReducer } from "./likes";


export default combineReducers({ 
    auth: authReducer,
    postMessage: postMessageReducer,
    deleteMessage: deleteMessageReducer,
    getMessageFeed: getMessageFeedReducer,
    getProfileMessages: getProfileMessagesReducer,
    user: usersReducer,
    likeMessage: likeMessageReducer,
    dislikeMessage: dislikeMessageReducer
});
