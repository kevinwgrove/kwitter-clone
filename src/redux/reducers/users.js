// // TODO: implement

import {
    CREATE_USER,
    CREATE_USER_SUCCESS,
    CREATE_USER_FAILURE,
    DELETE_USER,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    PUT_USER_PICTURE,
    PUT_USER_PICTURE_SUCCESS,
    PUT_USER_PICTURE_FAILURE,
    UPDATE_USER,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE
} from "../actions/index";

const INITIAL_STATE = {
    username: "",
    password: "",
    displayName: "",
    loading: false, 
    picture: null
}


export const usersReducer = (state = { ...INITIAL_STATE }, action) => {
    switch (action.type) {
        case CREATE_USER:
            return {
                ...INITIAL_STATE,
                loading: false,
            };
        case CREATE_USER_SUCCESS:
            const { text } = action.payload;
            return {
                ...INITIAL_STATE,
                text,
                loading: false,
            };
        case CREATE_USER_FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false,
            };
        case DELETE_USER:
            return {
                ...INITIAL_STATE,
                loading: true,
            };
        case DELETE_USER_SUCCESS:
            const { id } = action.payload;
            return {
                ...INITIAL_STATE,
                id,
                loading: false
            };
        case DELETE_USER_FAILURE:
            return {
                ...INITIAL_STATE,
                loading: false,
                error: action.payload
            }
        case PUT_USER_PICTURE:
            return {
                ...INITIAL_STATE,
                loading: true,
            }
        case PUT_USER_PICTURE_SUCCESS:
            return {
                ...INITIAL_STATE,
                picture: action.payload,
                loading: false
            }
        case PUT_USER_PICTURE_FAILURE:
            return {
                ...INITIAL_STATE,
                loading: false,
                error: action.payload
            }
        case UPDATE_USER:
            return {
                ...INITIAL_STATE,
                loading: false,
            };
        case UPDATE_USER_SUCCESS:
            const {  } = action.payload;
            return {
                ...INITIAL_STATE,
                text,
                loading: false,
            };
        case UPDATE_USER_FAILURE:
            return {
                ...INITIAL_STATE,
                error: action.payload,
                loading: false,
            };
        default:
          return state;
    }};