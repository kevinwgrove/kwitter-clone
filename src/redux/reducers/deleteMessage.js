// TODO: implement
import { 
    DELETE_MESSAGE,
    DELETE_MESSAGE_FAILURE,
    DELETE_MESSAGE_SUCCESS } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {

    payload: null,
    loading: false, 
    error: null

};

export const deleteMessageReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DELETE_MESSAGE:
        return {
            ...state,
            loading: true
        };
    case DELETE_MESSAGE_SUCCESS:
        const { id } = action.payload;
        return {
            ...state,
            payload: id,
            loading: false
        };
    case DELETE_MESSAGE_FAILURE:
        return {
            ...state,
            loading: false, 
            error: action.payload
        }
    default:
      return state;
  }
};