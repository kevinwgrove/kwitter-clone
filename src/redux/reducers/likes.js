// TODO: implement
import { LIKE_MESSAGE, 
    LIKE_MESSAGE_FAILURE, 
    LIKE_MESSAGE_SUCCESS,
    DISLIKE_MESSAGE, 
    DISLIKE_MESSAGE_FAILURE, 
    DISLIKE_MESSAGE_SUCCESS } from "../actions";

    const INITIAL_STATE = {
        loading: false,
        messageId: null,
        likeId: null,
        error: null,
    };

    export const likeMessageReducer = (state = INITIAL_STATE, action) => {
        switch (action.type) {
            case LIKE_MESSAGE:
                return {
                    ...state,
                    loading: true
                };
            case LIKE_MESSAGE_SUCCESS:
                 const {likeId, messageId} = action.payload;
                return {
                    ...state,
                    likeId: likeId,
                    messageId: messageId,
                    loading: false
                };
            case LIKE_MESSAGE_FAILURE:
                
                return {
                    ...state,
                    loading: false,
                    error: action.payload
                };
            default:
                return state;
        }
    }

    export const dislikeMessageReducer = (state = INITIAL_STATE, action) => {
        switch (action.type) {
            case DISLIKE_MESSAGE:
                return {
                    ...state,
                    loading: true
                };
            case DISLIKE_MESSAGE_SUCCESS:
                const {likeId, messageId} = action.payload;
                return {
                    ...state,                       
                    likeId: likeId,
                    messageId: messageId,
                    loading: false,
                    
                };
            case DISLIKE_MESSAGE_FAILURE:
                return {
                    ...state,
                    loading:false,
                    error: action.payload
                };
            default:
                return state;
        }
    }