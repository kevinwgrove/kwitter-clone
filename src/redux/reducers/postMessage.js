// TODO: implement
import { POST_MESSAGE, 
    POST_MESSAGE_FAILURE, 
    POST_MESSAGE_SUCCESS } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  payload: null, 
  loading: false, 
  error: null

};

export const postMessageReducer = (state = INITIAL_STATE , action) => {
  switch (action.type) {
    case POST_MESSAGE:
        return {
            ...state,
            loading: true
        };
    case POST_MESSAGE_SUCCESS:
        const { text } = action.payload;
        return {
            ...state,
            payload: text, 
            loading: false
        };
    case POST_MESSAGE_FAILURE:
        return {
            ...state,
            loading: false, 
            error: action.payload
        };
    default:
      return state;
  }
};