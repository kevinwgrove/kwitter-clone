// TODO: implement
import {
    GET_MESSAGE_FEED,
    GET_MESSAGE_FEED_FAILURE,
    GET_MESSAGE_FEED_SUCCESS } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  messageFeed: [], 
  loading: false, 
  error: null
};

export const getMessageFeedReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_MESSAGE_FEED:
        return {
            ...state,
            loading: true
        }
    case GET_MESSAGE_FEED_SUCCESS:
        const { messages } = action.payload
        return {
            ...state,
            messageFeed: messages, 
            loading: false
        }
    case GET_MESSAGE_FEED_FAILURE:
        return {
            ...state,
            loading: false, 
            error: action.payload
        }
    default:
      return state;
  }
};