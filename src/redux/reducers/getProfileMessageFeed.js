// TODO: implement
import {
    GET_PROFILE_MESSAGE_FEED,
    GET_PROFILE_MESSAGE_FEED_FAILURE,
    GET_PROFILE_MESSAGE_FEED_SUCCESS } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  profileMessages: [], 
  loading: false, 
  error: null
};

export const getProfileMessagesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROFILE_MESSAGE_FEED:
        return {
            ...state,
            loading: true
        }
    case GET_PROFILE_MESSAGE_FEED_SUCCESS:
        const { messages } = action.payload
        return {
            ...state,
            profileMessages: messages, 
            loading: false
        }
    case GET_PROFILE_MESSAGE_FEED_FAILURE:
        return {
            ...state,
            loading: false, 
            error: action.payload
        }
    default:
      return state;
  }
};