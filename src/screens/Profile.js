import React from "react";
import { MenuContainer } from "../components/menu";
import { Profile } from '../components/profile/Profile'
import { ProfileMessages } from '../components/profile-messages/ProfileMessages'
import { v4 as uuidv4 } from 'uuid'

export const ProfileScreen = () => (
  <>
    <MenuContainer />
    <h2>Profile</h2>
    <Profile/>
    <ProfileMessages/>
  </>
);
