import React from 'react'
import { Message } from '../components/message/Message'
import { MessageFeed } from '../components/message-feed/MessageFeed'
import { MenuContainer } from '../components/menu'

export const MessageFeedScreen = () => (
    <>
        <MenuContainer />
        <Message />
        <MessageFeed />
    </>
);