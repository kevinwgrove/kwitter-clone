import React from "react";
import { LoginFormContainer } from "../components/login-form";
import { MenuContainer } from '../components/menu/';
import { Link } from 'react-router-dom'
import { Button } from "@material-ui/core";
import './Home.css'
export const HomeScreen = () => {

  return (
    <div>
      <MenuContainer />
      <div id='home-screen'>
      <h2>Lot's favorite microblogging platform</h2>
      <LoginFormContainer />

      <Button type="submit" 
         variant="contained"
         style={{marginTop: 40+'px'}}>
           
      <Link 
      to='/registration'
      style={{ textDecoration: 'none', color: "black"}}
      > Register New User </Link>
      </Button>

      </div>
    </div>
  )
};
